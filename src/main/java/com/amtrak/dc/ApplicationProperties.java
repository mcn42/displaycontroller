/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc;

import java.awt.Color;

/**
 *
 * @author 00827843
 */
public class ApplicationProperties {
    public static final long REFRESH_PERIOD_MILLIS = 250L;
    public static final Color DISPLAY_BACKGROUND_COLOR = Color.GRAY;
    public static final int DEFAULT_FRAME_WIDTH = 400;
    public static final int DEFAULT_FRAME_HEIGHT = 400;
    
    public static final int INITIAL_ZOOM_LEVEL = 1;
}
