/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.amtrak.dc;

import com.amtrak.dc.drawable.DCText;
import com.amtrak.dc.drawable.DCText;
import com.amtrak.dc.gui.DCFrame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 00827843
 */
public class DisplayController {

    private ArrayList<DCFrame> frames = new ArrayList<>();
    private ArrayList<DCText> scene1 = new ArrayList<>();
    private ArrayList<DCText> scene2 = new ArrayList<>();
    private ArrayList<DCText> scene3 = new ArrayList<>();
    private ArrayList<DCText> scene4 = new ArrayList<>();
    private ArrayList[] scenes = {scene1, scene2, scene3, scene4};

    private ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();

    public DisplayController() {

        Dimension d = new Dimension(ApplicationProperties.DEFAULT_FRAME_WIDTH, ApplicationProperties.DEFAULT_FRAME_HEIGHT);
        Point[] points = {new Point(20, 20),
            new Point(20, 440),
            new Point(440, 20),
            new Point(440, 440)};
        this.createScenes();
        for (int i = 0; i < 4; i++) {
            String name = String.format("Frame-%d", i + 1);
            DCFrame fr = new DCFrame();
            fr.setTitle(name);
            this.addSceneToFrame(scenes[i], fr);

            fr.setSize(d);
            fr.setLocation(points[i]);
            this.frames.add(fr);
        }
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            for (DCFrame fr : frames) {
                fr.setVisible(true);
            }
        });

        this.exec.scheduleAtFixedRate(() -> this.animateStep(), 0, 250, TimeUnit.MILLISECONDS);

        while (true) {
            try {
                System.in.read();
                System.exit(0);
            } catch (IOException ex) {
                Logger.getLogger(DisplayController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void createScenes() {
        int zoom = 1;
        DCText c1 = new DCText(zoom, "Device A1");
        c1.colorTo(Color.GREEN);
        c1.setLocation(50, 50);
        scene1.add(c1);

        c1 = new DCText(zoom, "Device A2");
        c1.colorTo(Color.BLUE);
        c1.setLocation(50, 100);
        scene1.add(c1);

        c1 = new DCText(zoom, "Device A3");
        c1.colorTo(Color.RED);
        c1.setLocation(100, 50);
        scene1.add(c1);

        c1 = new DCText(zoom, "Device B1");
        c1.colorTo(Color.CYAN);
        c1.setLocation(50, 50);
        scene2.add(c1);

        c1 = new DCText(zoom, "Device B2");
        c1.colorTo(Color.MAGENTA);
        c1.setLocation(50, 100);
        scene2.add(c1);

        c1 = new DCText(zoom, "Device B3");
        c1.colorTo(Color.YELLOW);
        c1.setLocation(100, 50);
        scene2.add(c1);

        c1 = new DCText(zoom, "Device C1");
        c1.colorTo(Color.GREEN);
        c1.setLocation(50, 50);
        scene3.add(c1);

        c1 = new DCText(zoom, "Device C2");
        c1.colorTo(Color.BLUE);
        c1.setLocation(50, 100);
        scene3.add(c1);

        c1 = new DCText(zoom, "Device C3");
        c1.colorTo(Color.RED);
        c1.setLocation(100, 50);
        scene3.add(c1);

        c1 = new DCText(zoom, "Device D1");
        c1.colorTo(Color.CYAN);
        c1.setLocation(50, 50);
        scene4.add(c1);

        c1 = new DCText(zoom, "Device D2");
        c1.colorTo(Color.MAGENTA);
        c1.setLocation(50, 100);
        scene4.add(c1);

        c1 = new DCText(zoom, "Device D3");
        c1.colorTo(Color.YELLOW);
        c1.setLocation(100, 50);
        scene4.add(c1);
    }

    private void addSceneToFrame(ArrayList<DCText> scene, DCFrame frame) {
        for (DCText obj : scene) {
            frame.getGraphicsPanel().add(obj);
        }
    }

    private void animateStep() {
        java.awt.EventQueue.invokeLater(() -> {
            this.scene1.forEach((c) -> {
                c.translate(2, 0);
                c.repaint();
            });
            for (DCFrame fr : frames) {
                fr.refresh();
            }
        });
    }

    public static void main(String[] args) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            Log.general().log(java.util.logging.Level.SEVERE, "Could not set Look & Feel", ex);
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("Font families:\n");
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment(); 
        String[] familynames = env.getAvailableFontFamilyNames();
        for(String s: familynames) {
            sb.append(s).append("\n");
        }
        Log.general().info(sb.toString());
        
        DisplayController dc = new DisplayController();

    }
}
