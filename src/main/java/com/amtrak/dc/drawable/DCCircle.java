/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc.drawable;

import com.amtrak.dc.ApplicationProperties;
import com.amtrak.dc.Log;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import javax.swing.border.LineBorder;

/**
 *
 * @author 00827843
 */
public class DCCircle extends BaseJDrawable implements Moveable, Colorable, Blinkable, Labelled {

    private boolean blink;
    private long blinkStart = 0;
    private long blinkPeriod = 0;
    private boolean hide = false;
    private Ellipse2D circle = new Ellipse2D.Float();
    //private Rectangle2D s = new Rectangle2D.Float(0,0,20,20);
    private Font font = new Font("Arial", Font.PLAIN, 12);
    private Color color = Color.WHITE;
    private Stroke stroke = new BasicStroke(2.0f);
    AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, 0.75f);
    int radius;
    private double value = 50.0;
    private double maxValue = 100.0;
    private double minValue = 0.0;

    public DCCircle(int zoomLevel, String name) {
        super(name);
        this.configureZoomSizes();
        this.setZoomLevel(zoomLevel);
        this.createCircle();
    }

    private void createCircle() {
        float dia = Float.valueOf(this.getWidth());
        this.circle = new Ellipse2D.Float((float) this.getLocation().getX(), (float) this.getLocation().getY(), dia, dia);
        Rectangle r = new Rectangle(this.getBounds().x, this.getBounds().y, this.getBounds().width, this.getBounds().height + 30);
        this.setBounds(r);
        Dimension d = new Dimension(r.width,r.height);
        this.setPreferredSize(d);
        String msg = String.format("Creating bounds, size: %s, %s", r,d );
        Log.general().info(msg);
        this.setForeground(color);
        this.setBorder(new LineBorder(Color.WHITE,1));
    }

//    @Override
//    public void paintComponent(Graphics g) {
//        //Log.general().fine("Painting");
//        super.paintComponent(g);
//        Graphics2D g2 = (Graphics2D) g;
//        g2.setColor(color);
//        g2.setComposite(ac);
//        Rectangle2D.Double s = new Rectangle2D.Double(this.getX(),this.getY(),this.getWidth(),this.getHeight());
//        g2.fill(s);
//        g2.fill(circle);
//        
//
//    }
    
    @Override
    public void paintComponent(Graphics g) {
    Graphics2D g2 = (Graphics2D)g;
    int tick = 10;
    radius = getSize( ).width / 2 - tick;
    g2.setPaint(getForeground().darker( ));
    g2.drawLine(radius * 2 + tick / 2, radius,
        radius * 2 + tick, radius);
    g2.setStroke(new BasicStroke(2));
    draw3DCircle(g2, 0, 0, radius, true);
    int knobRadius = radius / 7;
        
    double th = value * (2 * Math.PI) / (maxValue - minValue);
    int x = (int)(Math.cos(th) * (radius - knobRadius * 3)),
    y = (int)(Math.sin(th) * (radius - knobRadius * 3));
    g2.setStroke(new BasicStroke(1));
    draw3DCircle(g2, x + radius - knobRadius,
                 y + radius - knobRadius, knobRadius, false );
  }

  private void draw3DCircle( Graphics g, int x, int y,
                             int radius, boolean raised) {
    Color foreground = getForeground( );
    Color light = foreground.brighter( );
    Color dark = foreground.darker( );
    g.setColor(foreground);
    g.fillOval(x, y, radius * 2, radius * 2);
    g.setColor(raised ? light : dark);
    g.drawArc(x, y, radius * 2, radius * 2, 45, 180);
    g.setColor(raised ? dark : light);
    g.drawArc(x, y, radius * 2, radius * 2, 225, 180);
  }

    private void drawText(Graphics2D g2) {
        g2.setFont(font);
        g2.setStroke(this.stroke);
        this.setForeground(Color.WHITE);

        this.setForeground(color);
    }

    @Override
    public void moveAbsolute(Point loc) {
        this.setLocation(loc);
    }

    @Override
    public void translate(int x, int y) {
        int xd = this.getX() + x;
        int yd = this.getY() + y;
        this.setLocation(xd, yd);
    }

    @Override
    public void colorTo(Color color) {
        this.color = color;
        this.setForeground(color);
    }

    @Override
    public void startBlink(float rate) {
        this.blink = true;
        this.blinkStart = System.currentTimeMillis();
        this.blinkPeriod = Float.valueOf(ApplicationProperties.REFRESH_PERIOD_MILLIS / rate).longValue();
    }

    @Override
    public void stopBlink() {
        this.blink = false;
        this.hide = false;
    }

    @Override
    public boolean isBlinking() {
        return this.blink;
    }

    private void handleBlink() {
        long time = System.currentTimeMillis();
        if ((time - this.blinkStart) >= this.blinkPeriod) {
            this.hide = !this.hide;
            this.blinkStart = time;
        }
    }

    @Override
    public String getText() {
        return this.getName();
    }

    @Override
    public void setText(String text) {
        this.setName(text);
    }

    protected final void configureZoomSizes() {
        this.addZoomSize(0, new Dimension(10, 15));
        this.addZoomSize(1, new Dimension(20, 27));
        this.addZoomSize(2, new Dimension(30, 40));
        this.addZoomSize(3, new Dimension(40, 50));
    }

    @Override
    public final void setZoomLevel(int level) {
        this.zoomLevel = level;
        Dimension d = this.zoomMap.get(level);
        if (d == null) {
            String msg = String.format("Drawable %s: can't find size for Level %d, using default", this.getName(), level);
            Log.general().severe(msg);
            d = new Dimension(20, 20);
            Font f = Fonts.getFontForZoomLevel(Fonts.BASIC_LABEL_SMALL, level);
            this.getGraphics().setFont(f);
        }
        this.setSize(d);

    }

}
