/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc.drawable;

import com.amtrak.dc.Log;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author 00827843
 */
public abstract class BaseDrawable implements Drawable, Zoomable {
    protected Graphics2D graphics;
    protected Point location;
    protected Point center;
    protected Dimension size;
    protected int zoomLevel;
    protected final String name;
    protected final Map<Integer,Dimension> zoomMap = new HashMap<>();
    
    public BaseDrawable(String name, Point location,int zoomLevel) {
        this.name = name;
        this.location = location;
        this.setZoomLevel(zoomLevel);
    }
    
    public BaseDrawable(String name, Graphics2D graphics, Point location,int zoomLevel) {
        this.name = name;
        this.graphics = graphics;
        this.location = location;
        this.setZoomLevel(zoomLevel);
    }
    
    @Override
    public abstract void draw();

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public Graphics2D getGraphics() {
        return graphics;
    }

    @Override
    public void setGraphics(Graphics2D graphics) {
        this.graphics = graphics;
    }
    
    @Override
    public final void setZoomLevel(int level) {
        this.zoomLevel = level;
        this.size = this.zoomMap.get(level);
        if(this.size == null) {
            Log.general().severe(String.format("Drawable %s: No size defined for Zoom Level %d",this.name, level));
            this.size = new Dimension(20,20);
        }
        this.caclulateCenter();
    }

    public Point getCenter() {
        return center;
    }

    @Override
    public String getName() {
        return name;
    }
    
    protected void caclulateCenter() {
        this.center = new Point();
        double x = (this.size.getWidth() / 2.0) + this.location.getX();
        double y = (this.size.getHeight() / 2.0) + this.location.getY();
        this.center.setLocation(x, y);
    }
    
    @Override
    public void addZoomSize(int level, Dimension size) {
        Dimension old = this.zoomMap.put(level, size);
        if(old != null) {
            String msg = String.format("Drawable %s replacing Zoom Level %d size %s with %s",this.name ,level, old, size);
        }
    }
    
}
