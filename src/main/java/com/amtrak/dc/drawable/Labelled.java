/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.amtrak.dc.drawable;

import java.awt.Font;

/**
 *
 * @author 00827843
 */
public interface Labelled {
    String getText();
    void setText(String text);
    Font getFont();
    void setFont(Font font);
}
