/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.amtrak.dc.drawable;

/**
 *
 * @author 00827843
 */
public interface Blinkable {
    void startBlink(float rate);
    void stopBlink();
    boolean isBlinking();
}
