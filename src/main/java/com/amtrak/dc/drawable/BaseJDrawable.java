/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc.drawable;

import com.amtrak.dc.ApplicationProperties;
import com.amtrak.dc.Log;
import java.awt.Dimension;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;

/**
 *
 * @author 00827843
 */
public abstract class BaseJDrawable extends JComponent implements Zoomable {
    protected int zoomLevel;
    protected Point center;
    
    protected final Map<Integer,Dimension> zoomMap = new HashMap<>();

    public BaseJDrawable(String name) {
        super();
        this.setName(name);
        this.setBackground(ApplicationProperties.DISPLAY_BACKGROUND_COLOR);
        this.setOpaque(true);
    }   
    
    @Override
    public abstract void setZoomLevel(int level);

    protected void caclulateCenter() {
        this.center = new Point();
        double x = (this.getWidth() / 2.0) + this.getX();
        double y = (this.getHeight() / 2.0) + this.getY();
        this.center.setLocation(x, y);
    }
    
    @Override
    public void addZoomSize(int level, Dimension size) {
        Dimension old = this.zoomMap.put(level, size);
        if(old != null) {
            String msg = String.format("JDrawable %s replacing Zoom Level %d size %s with %s",this.getName() ,level, old, size);
            Log.general().warning(msg);
        }
    }
    
}
