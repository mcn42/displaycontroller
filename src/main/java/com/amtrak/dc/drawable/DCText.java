/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc.drawable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;

/**
 *
 * @author michaeln
 */
public class DCText extends JComponent implements Colorable, Moveable {
    private Dimension preferredSize = new Dimension();
    private String text;
    private Font font = new Font("Arial",Font.PLAIN,12);
    private Float textX;
    private Float textY;
    private boolean sized = false;
    private Color color = Color.WHITE;
    
    public DCText(String text) {
        super();
        this.text = text;
    }
    
    public DCText(int zoom, String text) {
        super();
        this.text = text;
    }

    @Override
    public Dimension getPreferredSize() {
        return this.preferredSize;
    }    

    @Override
    public void setVisible(boolean vis) {
        super.setVisible(vis);
        if(vis) {
            this.calculateSize(((Graphics2D)this.getGraphics()).getFontRenderContext());
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(this.getForeground());
        g2.setFont(font); 
        
        if(!this.sized) {
            FontRenderContext frc = g2.getFontRenderContext();
            this.calculateSize(frc); 
            this.sized = true;
        }      
        
        g2.drawString(this.text, this.textX, this.textY);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        Graphics2D g2 = (Graphics2D) this.getGraphics();
        if(g2 != null) {
            FontRenderContext frc = g2.getFontRenderContext();
        this.calculateSize(frc);
        }       
    }
    
    private void calculateSize(FontRenderContext frc) {      
        Rectangle2D bounds = this.font.getStringBounds(this.text, frc); 
        LineMetrics metrics = this.font.getLineMetrics(this.text, frc); 
        float width = (float) bounds.getWidth(); // The width of our text
        float lineheight = metrics.getHeight(); // Total line height 
        float ascent = metrics.getAscent(); // Top of text to baseline 
        // Now display the message centered horizontally and vertically in box 
        Rectangle2D box = new Rectangle2D.Double(bounds.getX(),bounds.getY(), bounds.getWidth() + 2, bounds.getHeight() + 2);
        textX = (float) (box.getX() + (box.getWidth() - width)/2); 
        textY = (float) (box.getY() + (box.getHeight() - lineheight)/2 + ascent); 
        this.setPreferredSize(new Dimension((int)box.getX(),(int)box.getY()));
    }

    @Override
    public void colorTo(Color color) {
        this.setForeground(color);
        this.color = color;
    }

    @Override
    public void moveAbsolute(Point loc) {
        super.setLocation(loc);
    }

    @Override
    public void translate(int x, int y) {
        Point p = new Point(this.getLocation().x + x, this.getLocation().y + y);
        super.setLocation(p);
    }
}
