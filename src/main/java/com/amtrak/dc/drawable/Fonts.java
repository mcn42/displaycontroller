/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc.drawable;

import com.amtrak.dc.Log;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

/**
 *  This class provides fixed Font styles and sizes for consistency
 * @author 00827843
 */
public enum Fonts {
    BASIC_LABEL_SMALL, BASIC_LABEL_LARGE;
    
    private static final Map<Fonts,Map<Integer,Font>> fontMap = new HashMap<>();
    static {
        configureFontMaps();
    }
    
    private static void configureFontMaps() {
        Font baseDialog = Font.getFont(Font.DIALOG);
        HashMap<Integer,Font> map = new HashMap<>();
        map.put(0, baseDialog.deriveFont(Font.PLAIN, 6.0f));
        map.put(1, baseDialog.deriveFont(Font.PLAIN, 8.0f));
        map.put(2, baseDialog.deriveFont(Font.PLAIN, 10.0f));
        map.put(3, baseDialog.deriveFont(Font.PLAIN, 12.0f));
        fontMap.put(BASIC_LABEL_SMALL, map);
        
        map = new HashMap<>();
        map.put(0, baseDialog.deriveFont(Font.PLAIN, 9.0f));
        map.put(1, baseDialog.deriveFont(Font.PLAIN, 12.0f));
        map.put(2, baseDialog.deriveFont(Font.PLAIN, 15.0f));
        map.put(3, baseDialog.deriveFont(Font.PLAIN, 18.0f));
        fontMap.put(BASIC_LABEL_LARGE, map);
    }
    
    public static Font getFontForZoomLevel(Fonts f, int level) {
        Map<Integer,Font> map = fontMap.get(f);
        Font font = map.get(level);
        if(font == null) {
            String msg = String.format("Can't find font for Level %d, Fonts %s; using default", level, f.toString());
            Log.general().severe(msg);
            font = Font.getFont(Font.DIALOG).deriveFont(Font.PLAIN, 12);
        }
        return font;
    }
}
