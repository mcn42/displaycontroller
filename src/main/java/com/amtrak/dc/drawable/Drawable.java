/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.amtrak.dc.drawable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;

/**
 *
 * @author 00827843
 */
public interface Drawable {
    Point getCurrentLocation();
    void draw();
    boolean isVisible();
    Graphics2D getGraphics();
    void setGraphics(Graphics2D graphics);
    Color getCurrentColor();
    Paint getPaint();
    void setPaint(Paint p);
    Dimension getCurrentSize();
    String getName();
}
