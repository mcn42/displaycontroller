package com.amtrak.dc.drawable;

import java.awt.Point;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */

/**
 *
 * @author 00827843
 */
public interface Moveable {    
    void moveAbsolute(Point loc);
    void translate(int x, int y);
}
