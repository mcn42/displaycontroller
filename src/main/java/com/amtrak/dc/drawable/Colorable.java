/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.amtrak.dc.drawable;

import java.awt.Color;

/**
 *
 * @author 00827843
 */
public interface Colorable {
    void colorTo(Color color);
    
}
