/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.amtrak.dc.drawable;

import java.awt.Dimension;

/**
 *
 * @author 00827843
 */
public interface Zoomable {
    void setZoomLevel(int level);
    void addZoomSize(int level, Dimension size);
}
