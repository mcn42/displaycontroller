/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.amtrak.dc;

import java.util.logging.Logger;

/**
 *
 * @author 00827843
 */
public class Log {
    private static final Logger baseLogger = Logger.getLogger("com.amtrak.dc");
    private static final Logger commandLogger = Logger.getLogger("com.amtrak.dc.mq");
    
    static {
        configureLoggers();
    }
    
    private static void configureLoggers() {
        
    }
    
    public static Logger general() {
        return baseLogger;
    }
    
    public static Logger commands() {
        return commandLogger;
    }
}
